import os
from flask import Flask, request, render_template
import requests
from app import config
from app.parser import startBankStatementParse

app = Flask(__name__)

parse_path=config.CONTEXT_PATH+'parsebankstatement'
print("parse_path CONTEXT_PATH  : "+parse_path)

@app.route(parse_path, methods=['POST'])
def parsebankstatement():
    
    parse_res=startBankStatementParse.entryBankStatementParser(request)
    response={}
    response.update({"status":"test success"})
    response.update(parse_res)
    return response

@app.route("/")
def index():
    return render_template("index2.html");   


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)