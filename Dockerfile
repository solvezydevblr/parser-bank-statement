FROM python:3.7-slim

# Set the working directory to app & Copy the current directory contents into the container at app
ARG APP_PATH=/usr/src/app/
WORKDIR ${APP_PATH}​
ADD . ${APP_PATH}​
EXPOSE 8080

RUN mkdir -p ${APP_PATH}/output
RUN apt-get update && \
	apt-get install -y --no-install-recommends tesseract-ocr && \
	apt-get install -y libsm6 libxext6 libxrender-dev build-essential

COPY requirement.txt requirement.txt
RUN pip install --no-cache-dir -r requirement.txt

RUN apt-get purge -y build-essential
RUN apt-get autoremove -y
RUN apt-get clean

CMD [ "gunicorn", "-b", "0.0.0.0:8080", "main:app", "--timeout", "90"]
#ENTRYPOINT ["python","main.py"]