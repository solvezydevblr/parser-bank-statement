import tabula.io
import pandas as pd
import numpy as np
from datetime import datetime as dt
import datetime
import math
import warnings
from dateutil import relativedelta
warnings.filterwarnings("ignore")
pd.set_option('display.max_columns', None)
today = datetime.date.today()

def sbiBankStParser(request):
    
    print("************Inside Parser Statement****************")
    print("Posted file: {}".format(request.files['file']))
    file_name = request.files['file']
    
    ## SBI
    table = tabula.read_pdf(file_name,pages="all",stream =True,
                        pandas_options={"header": [0, 1]},
                        relative_area=True,
                        multiple_tables=True)
    df_all=pd.DataFrame()
    for count in range(0,len(table)):
        if table[count].shape[1]>=4:
            df_all=pd.concat([df_all, table[count]], axis=0)
    df_all = df_all.dropna( how='all')
    df_all=df_all.drop_duplicates()
    new_header = df_all.iloc[0].str.upper() #grab the first row for the header
    df_all_new = df_all[1:] #take the data less the header row
    df_all_new.columns = new_header
    df_all_new=df_all_new.reset_index()
    df_all_new.drop(['index'],axis=1,inplace=True)
    
    
    
    df_json=df_all_new.to_json(orient='records')


    return 