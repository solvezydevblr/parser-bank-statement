from app.parser.banks_parse import Initialbanksparse

def entryBankStatementParser(request):
    
    bankName=request.args['bankName']
    
    if bankName=="SBI":
        Initialbanksparse.sbiBankStParser(request)
    else:
        bankName="ELSE"
        df_json="ELSE"
    return {"bankName": bankName,"parsed_res":df_json}
