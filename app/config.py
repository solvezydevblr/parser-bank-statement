import os

# basedir = os.path.abspath(os.path.dirname(__file__))

# class Config:
#     SENTRY_DSN = os.getenv("CORE_PYTHON_SENTRY_DSN")
#     FILE_UPLOAD_URL = os.getenv('OCR_FILE_UPLOAD_SERVICE_ENDPOINT')
#     DEBUG = False

# class DevelopmentConfig(Config):
#     DEBUG = True


# class TestingConfig(Config):
#     DEBUG = True
#     TESTING = True

# class PreProductionConfig(Config):
#     DEBUG = False

# class ProductionConfig(Config):
#     DEBUG = False


# config_by_name = dict(
#     dev=DevelopmentConfig,
#     test=TestingConfig,
#     prod=ProductionConfig,
#     preprod=PreProductionConfig
# )

# dsn = Config.SENTRY_DSN
# fileUploadUrl = Config.FILE_UPLOAD_URL

# activeEnvironmentVariables = Config
CONTEXT_PATH = '/bank-statement-parser-india/'
